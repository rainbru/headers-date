#!/usr/bin/env ruby
# coding: utf-8

#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

# Just to make sure we can find files in ../lib/ dir.
$LOAD_PATH << File.expand_path('../lib', File.dirname(__FILE__))

require 'OutputText'

require 'optparse'

$output = OutputText.new()

def get_gnu_header(filename)
  in_header = false
  header = ""

  # Reading file in read-only mode
  File.open(filename, "r") do |file|
    # For each line
    while content = file.gets
      if content.index("#")!=nil then # Handling ruby comments
         header += content
      end        
        
      if not in_header then
        if content.index("/*")!=nil then
          in_header = true
          header += content
        end
      else
        header += content
        if content.index("*/")!=nil then
          return header
        end
      end
    end
  end

  return header
end

def get_gnu_header_date(filename)
  header = get_gnu_header(filename)
  header.scrub! # Should fix "invalid-byte-sequence-in-utf-8" error
  
  pos1 = header.index("Copyright");
  if pos1 == nil then
    return "No Copyright found"
  end

  posC = header.index("(C)");
  if posC != nil then
    pos1 += 3
  end

  offset = pos1+10
  pos2 = header.index(/[A-Za-z]/, offset); # All other than number

  if (pos1==nil) or (pos2==nil) then
    return "Error"
  end

  pos1 += 10 # The copyright len
  ret = header[pos1, pos2 - pos1 - 1]
  if ret != nil then
    ret.delete!("\n")
    ret.delete!("* ")
    return ret
  else
    return pos1.to_s + "," + pos2.to_s
  end
end

## date :     The GNU header date
## filename : The filename
def add_date_filename(date, filename)
  if $output.verbose then
    if date == "No Copyright found" then
      return
    end
    if date == "Error" then
      return
    end
      
  end

  if $output.files[date] == nil then   # Hash key contains no other filename
    $output.files[date] = Array.new()  # Creating a new array for this value
  end

  $output.files[date].push( filename ) # In all cases, we add the filename

end

def contain_word(filename, word)
  header = get_gnu_header(filename)
  
  if header.index(word) == nil then
    return false
  else
    return true
  end
end

def handle_file(filename)
  if filename =~ /.*\.o$/ then 
    return
  end
  if filename =~ /.*\.a$/ then 
    return
  end
  if filename =~ /.*\.lo$/ then 
    return
  end
  if filename =~ /.*\.la$/ then 
    return
  end
  if filename =~ /.*\.Plo$/ then 
    return
  end
  if filename =~ /.*\.so.*/ then 
    return
  end

  date = get_gnu_header_date(filename)
  add_date_filename(date, filename)

  # The foobar mode implementation
  if $output.foobar then
    if contain_word(filename, 'Foobar') then
      $output.foobar_array.push(filename)
    end
  end
  
end

def handle_directory(dirname)
  return if File.symlink?(dirname)
  
  Dir.foreach(dirname){ |filename|
    fullname = dirname + filename

    if File.symlink?(filename) then
      next
    else 
      if File.directory?(fullname) then
        if filename=="." or filename ==".." then
#          print "Skipping . and ..\n"
          next
        else
          handle_directory(dirname + filename + '/')
        end
      else 
        # Removing backup file
        if filename.rindex("~") != nil then
          next
        else
          handle_file(fullname)
        end
      end
    end
  }
end

opts=OptionParser.new
opts.on('-h', '--help')   { show_help; exit 0;  }
opts.on('-v', '--verbose'){ $output.verbose=true; }
opts.on('-q', '--quiet')  { $output.quiet=true;   }
opts.on('-f', '--foobar') { $output.foobar=true;   }
opts.parse!(ARGV)

# the project directory name
project_dir = ARGV[0].to_s

if project_dir.empty? then
  print "You should add the project directory argument\n"
  print "try with the --help option\n"
  exit 1
else
  handle_directory(project_dir + "/")
end

# print result
$output.print

