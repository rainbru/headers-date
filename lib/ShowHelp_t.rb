# coding: utf-8
#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

require_relative './ShowHelp'

require_relative './CaptureOutput'

# A simple HeaderDate test
class TestHeaderDate < Test::Unit::TestCase
  # Test that the show_help print at least the 'usage' word
  def test_showHelp
    out, err = capture_output do
      show_help
    end
    assert_match(/usage/i, out)
    assert_equal "", err       # Should not print to stderr
  end
end

