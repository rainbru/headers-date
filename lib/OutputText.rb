# coding: utf-8
#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

require_relative 'OutputBase'

# Handle the final output of the program
#
# headers-date has different modes :
# - foobar: used to search for the "Foobar" string as project name;
# - quiet: a very consise way;
# - verbose: show all listed files.
class OutputText < OutputBase
  attr_accessor :verbose, :quiet, :foobar

  def initialize()
    super()
    @verbose = false;
    @quiet   = false;
    @foobar  = false;
  end

  # Print the actual enabled mode(s) (Quiet, Verbose or Foobar)
  def print_mode
    puts "Quiet mode enabled"   if @quiet
    puts "Verbose mode enabled" if @verbose
    puts "Foobar mode enabled"  if @foobar
  end

  # Print the content of @files hash
  #
  # Will print the hash length in normal mode or every files in the
  # hash if verbose mode is enabled.
  def print_files
    @files.each  do |key, array|
      if @verbose then
        puts("\n==>" + key + " (" + array.length.to_s + " file(s) found) :")
        array.each{|value|
          puts("  " + value )
        }
      else
        puts("\n==>" + key + " :")
        puts("  " + array.length.to_s + " file(s) found")
      end
    end

  end

  # Prints the content of the @foobar_array array
  def print_foobar
    puts("\n==> Foobar file list (" + @foobar_array.length.to_s + 
         " file(s) found) :")
      
    @foobar_array.each{|filename| puts(filename)  }
  end

  # Print the results
  def print()
    print_mode
    print_files
    print_foobar if @foobar
  end
  
end
