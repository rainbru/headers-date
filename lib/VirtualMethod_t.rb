# coding: utf-8
#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

require "test/unit"

require_relative 'VirtualMethod'

# Test cases from example implementation https://gist.github.com/mssola/6138163

# An abstract type with only virtual members
class VirtualClass
  virtual :vfoo, :foo
end

# A class overriding both virtual methods
class GoodBoy < VirtualClass
  # VirtualClass#vfoo override
  def vfoo
    # 
  end
  
  # VirtualClass#foo override
  def foo; end
end

# A subclass that doesn't override virtual members
class BadBoy < VirtualClass
end


# Here are the tests

# VirtualMethod unit tests
class TestVirtualMethod < Test::Unit::TestCase

  # Calling a virtual method shol raise an VirtualMethodError exception
  def test_notoverridden_method
    assert_raise(VirtualMethodError){
      BadBoy.new.vfoo
    }
  end

  # Calling an overriden virtual method shoudn't raise an exception
  def test_override
    GoodBoy.new.vfoo
  end
  
end
