# coding: utf-8
#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

require_relative 'VirtualMethod'

# Contains the data structures needed to compute results
class OutputBase
  
  attr_accessor :files, :foobar_array

  # The following pure virtual method must be overridden by subclasses
  virtual :print  
  
  def initialize()
    # Here is the fileame hash, containing arrays in date-related keys
    # "2007" -> [file1, file2],
    # "2008" -> Another array
    # etc...
    @files = Hash.new
    
    # Will contain the filename conainting the 'Foobar' word
    @foobar_array = Array.new
  end
  
end
