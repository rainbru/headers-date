# coding: utf-8
#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

# Show the usage text describing how to use the program
def show_help()
print "Usage :
  headers-date.rb [-v][-q] dir

  dir             The base directory of the project to be scanned.

  -v,--verbose    Verbose mode. Will prompt for file names.
  -q,--quiet      Remove filename where error occurs (No Copyright ...)
  -f,--foobar     Search for the Foobar word in the file
"
end
