# coding: utf-8
#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

require "test/unit"
#require 'test/zentest_assertions'

require_relative 'OutputText'
require_relative 'CaptureOutput'


# The OutputText class test case
class TestOutputText < Test::Unit::TestCase

  # Test if OutputText attributes correctly set to false in constructor
  def test_constructor_mode
    op = OutputText.new
    # Use assert instead of assert_false to make jruby happy
    assert(op.verbose == false)
    assert(op.quiet == false)
    assert(op.foobar == false)
  end

  # Make sure TextOutput contain a hash named 'files'
  def test_files_hash
    # Just make sure we can access the files method 
    assert(OutputText.method_defined? 'files')

    # Makes sure the returned object is a hash
    op = OutputText.new
    assert(op.files.instance_of?(Hash))
  end

  # Test the OutputText output in quiet mode
  def test_print_mode_quiet
    ot = OutputText.new
    ot.quiet = true
    out, err = capture_output do
      ot.print_mode
    end
    assert_match "Quiet", out
    assert_equal "", err
  end

  # Test the OutputText output with no files
  #
  # If no files were added, print_files should print nothing
  def test_print_files_none
    ot = OutputText.new
    out, err = capture_output do
      ot.print_files
    end
    assert_match "", out
    assert_equal "", err
  end

  # Test the number of files listed with some example files
  def test_print_files
    ot = OutputText.new
    # Just adding a file
    ot.files["2009"] = Array.new()
    ot.files["2009"].push( "test.rb" )
    ot.files["2009"].push( "test2.rb" )
    out, err = capture_output do
      ot.print_files
    end
    assert_match "2 file(s)", out
    assert_equal "", err
  end

  
  # Adding at least one file should print something
  def test_print_files_verbose
    ot = OutputText.new
    ot.verbose = true

    # Just adding a file
    ot.files["2007"] = Array.new()
    ot.files["2007"].push( "test.rb" )
    
    out, err = capture_output do
      ot.print_files
    end
    assert_match "test.rb", out
    assert_equal "", err
  end

  # Should print 0 file in foobar mode
  def test_print_foobar_none
    ot = OutputText.new

    out, err = capture_output do
      ot.print_foobar
    end
    assert_match "0 file", out
    assert_equal "", err
  end

  # Test the number files listed from the foobar array
  def test_print_foobar_one
    ot = OutputText.new
    # Just adding a file
    ot.foobar_array.push("test.rb")
    out, err = capture_output do
      ot.print_foobar
    end
    assert_match "1 file", out
    assert_match "test.rb", out
    assert_equal "", err
  end

  # With no file and non-verbose mode, output should be an empty string
  def test_print_empty
    ot = OutputText.new

    out, err = capture_output do
      ot.print
    end

    assert_equal "", out
    assert_equal "", err
  end

  # Fully test the print method
  def test_print_full
    ot = OutputText.new
    ot.foobar = true  

    ot.files["2009"] = Array.new()
    ot.files["2009"].push( "test.rb" )

    # Just adding a file
    out, err = capture_output do
      ot.print
    end
    assert_equal "", err

    # Foobar mode is enabled (print_mode and print_foobar is called)
    assert_match "Foobar mode enabled", out
    assert_match "Foobar file list (0 file", out
    # And should find the 2009 added file
    assert_match "2009 :\n  1 file", out
  end

end
