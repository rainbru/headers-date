# coding: utf-8
#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

# A pure virtual method implementation
#
# from https://gist.github.com/mssola/6138163
#
# Example :
# class VirtualClass
#  virtual :vfoo, :foo
# end
#
# Then reimplement in subclass
# class GoodBoy < VirtualClass
#   def vfoo
#     puts 'Yay!'
#   end
# end

class VirtualMethodError < RuntimeError
  ##
  # Just call to super with some fancy message.
  def initialize(name)
    super("Error: Pure virtual method '#{name}' called")
  end
end

##
# Here's the trick: let's open the Module class and implement the
# 'virtual' method, so it's available also for classes.
class Module
  ##
  # This method defines a method for each of the elements passed by the
  # variable length argument. The implementation for each method will be
  # just raising a VirtualMethodError coupled with the name of the method.
  def virtual(*methods)
    methods.each do |name|
      define_method(name) { raise VirtualMethodError, name }
    end
  end
end

