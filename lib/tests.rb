#!/usr/bin/env ruby
# coding: utf-8

# coding: utf-8
#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA


require './OutputText_t.rb'
require './VirtualMethod_t.rb'
require './ShowHelp_t.rb'
