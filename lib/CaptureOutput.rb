# coding: utf-8

# coding: utf-8
#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of headers-date.
#
#  headers-date is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  headers-date is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with headers-date; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

# From https://groups.google.com/forum/#!topic/capistrano/VRK3RYkZedU

# Captures $stdout and $stderr to StringIO objects and returns them.
# Restores $stdout and $stderr when done.
#
# Usage:
#   def test_puts
#     out, err = capture do
#       puts 'hi'
#       STDERR.puts 'bye!'
#     end
#     assert_equal "hi\n", out.string
#     assert_equal "bye!\n", err.string
#   end
def capture_output
  require 'stringio'
  orig_stdout = $stdout.dup
  orig_stderr = $stderr.dup
  captured_stdout = StringIO.new
  captured_stderr = StringIO.new
  $stdout = captured_stdout
  $stderr = captured_stderr
  yield
  captured_stdout.rewind
  captured_stderr.rewind
  return captured_stdout, captured_stderr
ensure
  $stdout = orig_stdout
  $stderr = orig_stderr
end
