#  Copyright 2009-2010, 2013, 2017-2018 Jérôme PASQUIER
# 
#  This file is part of GnuHeaderDate.
#
#  GnuHeaderDate is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  GnuHeaderDate is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GnuHeaderDate; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#  02110-1301  USA

clean:
	rm -f *~
	rm -f *.log

check:
	cd lib&&ruby tests.rb

# Results are in lib/coverage/index.html
coverage:
	cd lib&&ruby coverage.rb
	echo "=> Coverage results are in lib/coverage/index.html"
