# headers-date

A tool used to test the GNU headers' dates for a complete directory or project.

# Dependencies

If the error *`require': cannot load such file -- coveralls (LoadError)* occurs
while running unit tests, you're missing the *coveralls* gem :

	sudo apt install ruby-coveralls

# Documentation

To be able to generate code documentation, you will need `yard` :

	sudo apt install yard

Then, from the root directory, simple call `yard` and point your favorite
browser to *doc/index.html*.

# Usage

`header-date.rb` is a console-only tool used to hel developper change headers 
date once a year. It accepts the following command-line options :

	./header-date.rb [-v][-q] dir

	dir             The base directory of the project to be scanned.

	-v,--verbose    Verbose mode. Will prompt for file names.
	-q,--quiet      Remove filename where error occurs (No Copyright ...)
	-f,--foobar     Search for the Foobar word in the file


# Example output

Here is an example *verbose* call on its own sources directory :

	./headers-date.rb -v ~/programmation/github.com/headers-date/
	Verbose mode enabled

The provided output follow this layout :

	==>2009-2010,2013,2017 (2 file(s) found) :
	/home/rainbru/programmation/github.com/headers-date//bin/headers-date.rb
	/home/rainbru/programmation/github.com/headers-date//Makefile

	==>1975,1977,1979-1982,1984-1989,1992,1994 (1 file(s) found) :
	/home/rainbru/programmation/github.com/headers-date//essai03.txt

	==>2006 (1 file(s) found) :
	/home/rainbru/programmation/github.com/headers-date//essai01.txt

	==>1998,1999,2003 (1 file(s) found) :
	/home/rainbru/programmation/github.com/headers-date//essai02.txt

	==> (1 file(s) found) :
	/home/rainbru/programmation/github.com/headers-date//.git/hooks/pre-rebase.sample
